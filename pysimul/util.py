from pathlib import Path

path_tmp = Path("/tmp/fluidsim_figs")
path_tmp.mkdir(exist_ok=True)

def savefig(fig, name):
    path = path_tmp / name
    fig.savefig(path)
    print(f"Saving file {path}")
